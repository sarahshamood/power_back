<?php

namespace App\Http\Controllers;

use App\Models\Usage;
use App\Models\UserService;
use http\Env\Response;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class UsageController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function index()
    {
        $usage = DB::table('usage')
            ->join('user_service','user_service.id','=','usage.service_user_id')
            ->where('user_service.user_id',Auth::user()->id)
            ->select('usage.*','user_service.used')
            ->get()
        ;
        return Response()->json(['usage' => $usage]);
    }


}
