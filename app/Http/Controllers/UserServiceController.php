<?php

namespace App\Http\Controllers;

use App\Models\City;
use App\Models\Service;
use App\Models\SubService;
use App\Models\Usage;
use App\Models\UserService;
use Exception;
use http\Env\Response;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;

class UserServiceController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function index()
    {
        $services = DB::table('user_service')
            ->where('user_id',Auth::user()->id)
            ->where('done',false)
            ->join('sub_service','sub_service.id','=','user_service.sub_service_id')
            ->select('user_service.*','sub_service.value as max')
            ->get();

        // real time simulation
        foreach($services as $service){
            $temp = UserService::find($service->id);
            // edit here to controle
            if(rand(0,15) % 2 == 0 ) {
                $temp->used += $service->max * rand(1, 8) / 100;
                $service->used = $temp->used;
                $usage = Usage::create([
                    'amount' => $temp->used,
                    'service_user_id' => $service->id
                ]);
                $usage->save();
                $temp->save();
            }
            else
                $service->used = $temp->used;
        }

        return Response()->json(['services' => $services,]);
    }


    public function payAll(){
        $user = Auth::user();
        $services = $user->services;
        $total = 0;
        foreach ($services as $service){
            if($service->done)
                continue;
            $watt_price = $service->subService->service->watt_price;
            $bill = $service->used * $watt_price;
            $total += $bill;
            $service = UserService::find($service->id);
            $service->done = true;
            $service->save();
            $sub_service = SubService::find($service->subService->id);
            $sub_service->available++;
            $sub_service->save();
        }
        $user->account -= $total;
        $user->save();
        return Response()->json([
            'total' => $total,
        ]);
    }

    public function payOne($id){
        $user = Auth::user();
        $service = UserService::find($id);

        if(!$service)
            return Response()->json(['error' => 'no service with this ID.']);

        if($service->user_id != Auth::id())
            return Response()->json(['error' => 'this service is not yours.']);

        $total = 0;
            if(!$service->done){
            $watt_price = $service->subService->service->watt_price;
            $bill = $service->used * $watt_price;
            $total += $bill;
            $service = UserService::find($service->id);
            $service->done = true;
            $service->save();
            $sub_service = SubService::find($service->subService->id);
            $sub_service->available++;
            $sub_service->save();
        }
        $user->account -= $total;
        $user->save();
        return Response()->json([
            'total' => $total,
        ]);
    }

    public function store(Request $request){
        $data = Validator::make(
            $request->all(),
            [
                'sub_service_id' => 'required|int',
            ]
        );
        if($data->fails())
            return Response()->json(['errors'=>$data->errors()]);


        try {
            $service = SubService::where('id', $request['sub_service_id'])->firstOrFail();
        }
        catch (Exception $e){
            return Response()->json(['error'=>'service id is invalid.']);
        }

        if(Auth::user()->city_id != $service->city_id)
            return Response()->json(['error'=>'this service is not for your city.']);

        $user_service = UserService::create([
            'sub_service_id' => $request['sub_service_id'],
            'user_id' => Auth::id()
        ]);

        return Response()->json(['service' => $user_service]);
    }

}
