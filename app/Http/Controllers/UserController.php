<?php

namespace App\Http\Controllers;

use App\Models\City;
use App\Models\User;
use Exception;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;

class UserController extends Controller
{

    public function register(Request $request){
        $data = Validator::make(
            $request->all(),
            [
                'name' => 'required|string|max:255',
                'email'=> 'required|email|max:255',
                'password'=> 'required|string|max:50',
                'phone' => 'required|string|max:20',
                'city_id' => 'required|int',
            ]
        );


        if($data->fails())
            return Response()->json(['errors'=>$data->errors()]);

        try {
            $city = City::where('id', $request['city_id'])->firstOrFail();
        }
        catch (Exception $e){
            return Response()->json(['error'=>'city id is invalid.']);
        }

        try{
            $user = User::create([
                'city_id' => $request['city_id'],
                'name' => $request['name'],
                'email' => $request['email'],
                'password' => Hash::make($request['password']),
                'phone' => $request['phone']
            ]);
        }
        catch (Exception $exception){
            return Response()->json([
                'error'=>'email or phone number is invalid.',
            ]);
        }


        $token = $user->createToken('authToken')->plainTextToken;
        $city->users_count ++;
        $user->save();
        $city->save();
        return Response()->json([
            'user'=>$user,
            'token' => $token,
        ]);
    }

    public function login(Request $request){
        if (!Auth::attempt($request->only('email', 'password'))) {
            return response()->json([
                'error' => 'Invalid login details'
            ], 401);
        }

        $user = User::where('email', $request['email'])->firstOrFail();

        $token = $user->createToken('authToken')->plainTextToken;

        return response()->json([
            'token' => $token,
            'user' => $user
        ]);


    }

    public function logout(){
        try{
            Auth::user()->tokens->each(function ($token, $key) {
                $token->delete();
            });
            return Response()->json(['message'=>'log out successfully.']);
        }catch (Exception $exception){
            return Response()->json(['error'=>'log out failed.']);
        }
    }

}
