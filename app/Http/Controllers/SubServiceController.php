<?php

namespace App\Http\Controllers;

use App\Models\City;
use App\Models\Service;
use App\Models\SubService;
use Exception;
use http\Env\Response;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;

class SubServiceController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function index()
    {
        return Response()->json(['sub_services' => SubService::all()]);
    }

    public function city($id): \Illuminate\Http\JsonResponse
    {
        $city = City::find($id);
        if($city)
        return Response()->json([
            'sub_services' => DB::table('sub_service')->where('city_id',$id)
                ->join('services','services.id','=','sub_service.service_id')
                ->select('sub_service.*','services.type','services.watt_price')->get(),
            'city' => $city
            ]);
        return Response()->json(['error' => 'City id is invalid.']);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function store(Request $request)
    {
        $data = Validator::make(
            $request->all(),
            [
                'service_id' => 'required|int',
                'city_id' => 'required|int',
                'value' => 'required|int|min:1',
                'available' => 'required|int|min:1'
            ]
        );
        if($data->fails())
            return Response()->json(['errors'=>$data->errors()]);


        try {
            $city = City::where('id', $request['city_id'])->firstOrFail();
        }
        catch (Exception $e){
            return Response()->json(['error'=>'city id is invalid.']);
        }

        try {
            $service = Service::where('id', $request['service_id'])->firstOrFail();
        }
        catch (Exception $e){
            return Response()->json(['error'=>'service id is invalid.']);
        }


        $service = SubService::create(
            [
                'service_id' => $request['service_id'],
                'city_id' => $request['city_id'],
                'value' => $request['value'],
                'available' => $request['available'],
            ]
        );

        $service->save();
        return Response()->json([
            'sub_service' => $service
        ]);
    }

    public function show($id): \Illuminate\Http\JsonResponse
    {
        try {
            $sub_service = DB::table('sub_service')->where('sub_service.id',$id)
                ->join('services','services.id','=','sub_service.service_id')->select('sub_service.*','services.type','services.watt_price')->get()->firstOrFail();

            return Response()->json([
                'sub_service' => $sub_service,
            ]);
        }catch (Exception $e){
            return Response()->json([
                'error' => 'sub service not found'
            ]);
        }
    }



}
