<?php

namespace App\Http\Controllers;

use App\Models\Service;
use http\Env\Response;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class ServiceController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function index()
    {
        return Response()->json(['services' => Service::all()]);
    }



    public function store(Request $request)
    {
        $data = Validator::make(
            $request->all(),
            [
                'type' => 'required|string|max:255',
                'watt_price'=> 'required|int|min:1',
            ]
        );
        if($data->fails())
            return Response()->json(['errors'=>$data->errors()]);


        $service = Service::create(
            [
               'type' => $request['type'],
               'watt_price' => $request['watt_price']
            ]
        );

        $service->save();
        return Response()->json([
            'service' => $service
        ]);
    }


}
