<?php

namespace App\Http\Controllers;

use App\Models\Wallet;
use http\Env\Response;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;

class WalletController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function index()
    {
        $wallet = Wallet::where('user_id',Auth::user()->id)->get();
        return Response()->json([
            'wallet' => $wallet,
            'account' => Auth::user()->account
            ]);
    }

    public function store(Request $request)
    {
        $data = Validator::make(
            $request->all(),
            [
                'amount' => 'required|int|min:10',
            ]
        );

        if($data->fails())
        return Response()->json(['errors'=>$data->errors()]);

        $wallet = Wallet::create([
            'user_id' => Auth::user()->id,
            'amount' => $request['amount']
        ]);

        $wallet->save();
        $user = Auth::user();

        $user->account += $request['amount'];
        $user->save();
        return Response()->json([
            'wallet' => $wallet
        ]);
    }

}
