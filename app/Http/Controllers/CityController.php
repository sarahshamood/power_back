<?php

namespace App\Http\Controllers;

use App\Models\City;
use Exception;
use http\Env\Response;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class CityController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function index()
    {
        return Response()->json(['cities' => City::all()]);
    }


    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function store(Request $request)
    {
        $data = Validator::make(
            $request->all(),[
            'name' => 'required|string|min:2|max:20',
            ]);
        if($data->fails())
            return Response()->json(['errors'=>$data->errors()]);

        try{
            $city = City::create([
                'name' => $request['name']
            ]);

            $city->save();

            return Response()->json(['city' => $city]);
        }
        catch (Exception $e){
        return Response()->json(['error' => 'city is already exist']);
    }

    }






}
