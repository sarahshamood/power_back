<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Usage extends Model
{
    use HasFactory;
    protected $table = 'usage';
    protected $fillable = [
        'id',
        'service_user_id',
        'amount'
    ];
}
