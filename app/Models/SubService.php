<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class SubService extends Model
{
    use HasFactory;
    protected $table = 'sub_service';
    protected $fillable = [
        'id',
        'service_id',
        'city_id',
        'value',
        'available'
    ];

    public function City(){
       return $this->belongsTo(City::class,'city_id');
    }

    public function service(){
       return $this->belongsTo(Service::class,'service_id');
    }


}
