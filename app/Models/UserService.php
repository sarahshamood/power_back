<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class UserService extends Model
{
    use HasFactory;
    protected $table = 'user_service';
    protected $fillable = [
        'id',
        'user_id',
        'sub_service_id',
        'used',
        'done'
    ];

    public function subService(){
        return $this->belongsTo(SubService::class,'sub_service_id');
    }

}
