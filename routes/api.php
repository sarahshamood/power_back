<?php

use App\Http\Controllers\CityController;
use App\Http\Controllers\ServiceController;
use App\Http\Controllers\SubServiceController;
use App\Http\Controllers\UsageController;
use App\Http\Controllers\UserController;
use App\Http\Controllers\UserServiceController;
use App\Http\Controllers\WalletController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:sanctum')->get('/user', function (Request $request) {
    return $request->user();
});


Route::post('/register',[UserController::class,'register']);
Route::post('/login',[UserController::class,'login']);

Route::group(['middleware'=>['auth:sanctum']],
    function (){
        Route::delete('/logout',[UserController::class,'logout']);
        Route::get('/usage',[UsageController::class,'index']);
        Route::prefix('/wallet')->group(function(){
            Route::get('/all',[WalletController::class,'index']);
            Route::post('/create',[WalletController::class,'store']);
        });
        Route::prefix('/service')->group(function(){
            Route::get('/my',[UserServiceController::class,'index']);
            Route::post('/by',[UserServiceController::class,'store']);
            Route::put('/payAll',[UserServiceController::class,'payAll']);
            Route::put('/pay/{id}',[UserServiceController::class,'payOne']);
        });
    }
);

Route::prefix('/service')->group(function(){
    Route::get('/all',[ServiceController::class,'index']);
    Route::post('/create',[ServiceController::class,'store']);
});

Route::prefix('/sub_service')->group(function(){
    Route::get('/all',[SubServiceController::class,'index']);
    Route::get('/city/{id}',[SubServiceController::class,'city']);
    Route::get('/show/{id}',[SubServiceController::class,'show']);
    Route::post('/create',[SubServiceController::class,'store']);
});

Route::prefix('/city')->group(function(){
    Route::get('/all',[CityController::class,'index']);
    Route::post('/create',[CityController::class,'store']);
});
